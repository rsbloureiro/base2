package br.com.base2.mantisProva.start.test;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.base2.mantisProva.util.StartBrowser;

public class UtilPageTest{

	protected WebDriver driver;
	protected WebDriverWait wait;
		
	private String pStartUrl;
	protected String pFonteDados;
	private String pBrowser;
	private String pStartHub;
	protected String pScreenshotPathStart;
	protected String pScreenshotPathIssues;

		
	@Before
	public void inicializa() throws IOException {
		
		InputStream propertiesStream = getClass().getResourceAsStream("/teste.properties");
		Properties prop = new Properties();
		prop.load(propertiesStream);
		
		
		//Start
		pBrowser            	= prop.getProperty("browser.name");		
		pStartUrl           	= prop.getProperty("start.url");
		pStartHub           	= prop.getProperty("start.hub");
		pFonteDados 			= prop.getProperty("fonteDados.path");
		pScreenshotPathStart	= prop.getProperty("screenshotsStart.path");
		pScreenshotPathIssues	= prop.getProperty("screenshotsIssues.path");		
				
		driver = StartBrowser.selecionarBrowser(pBrowser, pStartUrl, pStartHub);
//		driver.manage().window().maximize();
//		driver.manage().deleteAllCookies();
//		driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
//		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
//		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);						
	
	}

	
	@After
	public void finaliza() throws IOException {
		if(driver != null) {
			driver.quit();
		}
//		getDriver().quit();
		Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe");
		
	}
	
	public void ConfigDriver(String url) {
		
	}
	
}
