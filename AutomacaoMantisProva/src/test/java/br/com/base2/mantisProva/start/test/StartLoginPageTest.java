package br.com.base2.mantisProva.start.test;

import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.support.PageFactory;

import br.com.base2.mantisProva.start.LoginPage;
import br.com.base2.mantisProva.util.BaseTest;
import br.com.base2.mantisProva.util.ExcelUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StartLoginPageTest extends UtilPageTest{
		
	@Test
	public void TC01_LoginSenhaErrada() throws Exception {
//		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC01_LoginPage_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados,"LoginPage");
		String sUsuario  = ExcelUtils.getCellData(1, 1);
		String sSenha	 = ExcelUtils.getCellData(1, 2);
		String sMensagem = ExcelUtils.getCellData(1, 3);
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.identificarUsuario(sUsuario, sSenha);	
//		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC01_LoginPage_02_entradaUsuarioSenha");
		loginPage.logarSistema();
		assertTrue(loginPage.existe(sMensagem));
//		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC01_LoginPage_03_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou",1, 4);
//		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC01_LoginPage_04_finishTest");
	}
	
	@Test
	public void TC02_LoginSenhaBranco() throws Exception {
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC02_LoginPage_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados,"LoginPage");
		String sUsuario  = ExcelUtils.getCellData(2, 1);
		String sSenha	 = ExcelUtils.getCellData(2, 2);
		String sMensagem = ExcelUtils.getCellData(2, 3);
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.identificarUsuario(sUsuario, sSenha);	
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC02_LoginPage_02_entradaUsuarioSenha");
		loginPage.logarSistema();
		assertTrue(loginPage.existe(sMensagem));
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC02_LoginPage_03_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou",2, 4);
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC02_LoginPage_04_finishTest");
	}
	
	@Test
	public void TC03_LoginSenhaCorretos() throws Exception {
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC03_LoginPage_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados,"LoginPage");
		String sUsuario  = ExcelUtils.getCellData(3, 1);
		String sSenha	 = ExcelUtils.getCellData(3, 2);
		String sMensagem = ExcelUtils.getCellData(3, 3);
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.identificarUsuario(sUsuario, sSenha);	
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC03_LoginPage_02_entradaUsuarioSenha");
		loginPage.logarSistema();
		assertTrue(loginPage.existe(sMensagem));
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC03_LoginPage_03_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou",3, 4);
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC03_LoginPage_04_finishTest");
	}
		
	
}
