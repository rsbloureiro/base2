package br.com.base2.mantisProva.issues.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.support.PageFactory;

import br.com.base2.mantisProva.issues.MenuClass;
import br.com.base2.mantisProva.issues.ReportIssuePage;
import br.com.base2.mantisProva.start.LoginPage;
import br.com.base2.mantisProva.start.test.UtilPageTest;
import br.com.base2.mantisProva.util.BaseTest;
import br.com.base2.mantisProva.util.ExcelUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IssuesReportIssuePageTest extends UtilPageTest {
	
	@Before
	public void iniciaTeste() throws Exception{
		ExcelUtils.setExcelFile(pFonteDados,"LoginPage");
		String sUsuario  = ExcelUtils.getCellData(3, 1);
		String sSenha	 = ExcelUtils.getCellData(3, 2);
		String sMensagem = ExcelUtils.getCellData(3, 3);
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.identificarUsuario(sUsuario, sSenha);	
		loginPage.logarSistema();
		assertTrue(loginPage.existe(sMensagem));
		ExcelUtils.setCellData(pFonteDados, "Passou",3, 4);
		MenuClass menuClass = PageFactory.initElements(driver, MenuClass.class);
		menuClass.acessarReportIssue();
	}
	
	@Test
	public void TC01_CamposEmBranco() throws Exception {
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC01_ReportIssue_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados, "ReportIssuePage");
		String sCategory				= ExcelUtils.getCellData(1, 1);
		String sReproducibility			= ExcelUtils.getCellData(1, 2);
		String sSeverity				= ExcelUtils.getCellData(1, 3);
		String sPriority				= ExcelUtils.getCellData(1, 4);
		String sSelectProfile			= ExcelUtils.getCellData(1, 5);
		String sPlatform				= ExcelUtils.getCellData(1, 6);
		String sOS						= ExcelUtils.getCellData(1, 7);
		String sOSVersion				= ExcelUtils.getCellData(1, 8);
		String sAssignTo				= ExcelUtils.getCellData(1, 9);
		String sSummary					= ExcelUtils.getCellData(1, 10);
		String sDescription				= ExcelUtils.getCellData(1, 11);
		String sAdditionalInformation	= ExcelUtils.getCellData(1, 12);
		String sUploadFile				= ExcelUtils.getCellData(1, 13);
		String sViewStatus				= ExcelUtils.getCellData(1, 14);
		String sMensagem				= ExcelUtils.getCellData(1, 15);
		ReportIssuePage reportIssuePage = PageFactory.initElements(driver, ReportIssuePage.class);
		reportIssuePage.inserirCategoria(sCategory);
		reportIssuePage.inserirReprodutividade(sReproducibility);
		reportIssuePage.inserirSeveridade(sSeverity);
		reportIssuePage.inserirPrioridade(sPriority);
		if (sSelectProfile.isEmpty()){
			reportIssuePage.inserirPlataforma(sPlatform);
			reportIssuePage.inserirOS(sOS);
			reportIssuePage.inserirVersaoOS(sOSVersion);
		} else {
			reportIssuePage.inserirPerfil(sSelectProfile);
		}
		reportIssuePage.inserirAtribuicao(sAssignTo);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC01_ReportIssue_02_Campos001");
		reportIssuePage.inserirSumario(sSummary);
		reportIssuePage.inserirDescricao(sDescription);
		reportIssuePage.inserirInfoAdicional(sAdditionalInformation);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC01_ReportIssue_03_Campos002");
		reportIssuePage.selecionarArquivo(sUploadFile);
		reportIssuePage.selecionarVisaoSituacao(sViewStatus);
		reportIssuePage.naoContinuarRelatando();
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC01_ReportIssue_04_Campos003");
		reportIssuePage.registrarIssue();
		assertTrue(reportIssuePage.existe(sMensagem));
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC01_ReportIssue_05_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou",1, 16);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC01_ReportIssue_06_finishTest");
	}
	
	@Test
	public void TC02_CamposObrigatorios() throws Exception {
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC02_ReportIssue_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados, "ReportIssuePage");
		String sCategory				= ExcelUtils.getCellData(2, 1);
		String sReproducibility			= ExcelUtils.getCellData(2, 2);
		String sSeverity				= ExcelUtils.getCellData(2, 3);
		String sPriority				= ExcelUtils.getCellData(2, 4);
		String sSelectProfile			= ExcelUtils.getCellData(2, 5);
		String sPlatform				= ExcelUtils.getCellData(2, 6);
		String sOS						= ExcelUtils.getCellData(2, 7);
		String sOSVersion				= ExcelUtils.getCellData(2, 8);
		String sAssignTo				= ExcelUtils.getCellData(2, 9);
		String sSummary					= ExcelUtils.getCellData(2, 10);
		String sDescription				= ExcelUtils.getCellData(2, 11);
		String sAdditionalInformation	= ExcelUtils.getCellData(2, 12);
		String sUploadFile				= ExcelUtils.getCellData(2, 13);
		String sViewStatus				= ExcelUtils.getCellData(2, 14);
		String sMensagem				= ExcelUtils.getCellData(2, 15);
		ReportIssuePage reportIssuePage = PageFactory.initElements(driver, ReportIssuePage.class);
		reportIssuePage.inserirCategoria(sCategory);
		reportIssuePage.inserirReprodutividade(sReproducibility);
		reportIssuePage.inserirSeveridade(sSeverity);
		reportIssuePage.inserirPrioridade(sPriority);
		if (sSelectProfile.isEmpty()){
			reportIssuePage.inserirPlataforma(sPlatform);
			reportIssuePage.inserirOS(sOS);
			reportIssuePage.inserirVersaoOS(sOSVersion);
		} else {
			reportIssuePage.inserirPerfil(sSelectProfile);
		}
		reportIssuePage.inserirAtribuicao(sAssignTo);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC02_ReportIssue_02_Campos001");
		reportIssuePage.inserirSumario(sSummary);
		reportIssuePage.inserirDescricao(sDescription);
		reportIssuePage.inserirInfoAdicional(sAdditionalInformation);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC02_ReportIssue_03_Campos002");
		reportIssuePage.selecionarArquivo(sUploadFile);
		reportIssuePage.selecionarVisaoSituacao(sViewStatus);
		reportIssuePage.naoContinuarRelatando();
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC02_ReportIssue_04_Campos003");
		reportIssuePage.registrarIssue();
		assertTrue(reportIssuePage.existe(sMensagem));
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC02_ReportIssue_05_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou",2, 16);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC02_ReportIssue_06_finishTest");
	}
	
	@Test
	public void TC03_ComAnexoMenor5MB() throws Exception {
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC03_ReportIssue_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados, "ReportIssuePage");
		String sCategory				= ExcelUtils.getCellData(3, 1);
		String sReproducibility			= ExcelUtils.getCellData(3, 2);
		String sSeverity				= ExcelUtils.getCellData(3, 3);
		String sPriority				= ExcelUtils.getCellData(3, 4);
		String sSelectProfile			= ExcelUtils.getCellData(3, 5);
		String sPlatform				= ExcelUtils.getCellData(3, 6);
		String sOS						= ExcelUtils.getCellData(3, 7);
		String sOSVersion				= ExcelUtils.getCellData(3, 8);
		String sAssignTo				= ExcelUtils.getCellData(3, 9);
		String sSummary					= ExcelUtils.getCellData(3, 10);
		String sDescription				= ExcelUtils.getCellData(3, 11);
		String sAdditionalInformation	= ExcelUtils.getCellData(3, 12);
		String sUploadFile				= ExcelUtils.getCellData(3, 13);
		String sViewStatus				= ExcelUtils.getCellData(3, 14);
		String sMensagem				= ExcelUtils.getCellData(3, 15);
		ReportIssuePage reportIssuePage = PageFactory.initElements(driver, ReportIssuePage.class);
		reportIssuePage.inserirCategoria(sCategory);
		reportIssuePage.inserirReprodutividade(sReproducibility);
		reportIssuePage.inserirSeveridade(sSeverity);
		reportIssuePage.inserirPrioridade(sPriority);
		if (sSelectProfile.isEmpty()){
			reportIssuePage.inserirPlataforma(sPlatform);
			reportIssuePage.inserirOS(sOS);
			reportIssuePage.inserirVersaoOS(sOSVersion);
		} else {
			reportIssuePage.inserirPerfil(sSelectProfile);
		}
		reportIssuePage.inserirAtribuicao(sAssignTo);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC03_ReportIssue_02_Campos001");
		reportIssuePage.inserirSumario(sSummary);
		reportIssuePage.inserirDescricao(sDescription);
		reportIssuePage.inserirInfoAdicional(sAdditionalInformation);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC03_ReportIssue_03_Campos002");
		reportIssuePage.selecionarArquivo(sUploadFile);
		reportIssuePage.selecionarVisaoSituacao(sViewStatus);
		reportIssuePage.naoContinuarRelatando();
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC03_ReportIssue_04_Campos003");
		reportIssuePage.registrarIssue();
		assertTrue(reportIssuePage.existe(sMensagem));
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC03_ReportIssue_05_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou",3, 16);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC03_ReportIssue_06_finishTest");
	}

	@Test
	public void TC04_ComAnexoMaior5MB() throws Exception {
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC04_ReportIssue_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados, "ReportIssuePage");
		String sCategory				= ExcelUtils.getCellData(4, 1);
		String sReproducibility			= ExcelUtils.getCellData(4, 2);
		String sSeverity				= ExcelUtils.getCellData(4, 3);
		String sPriority				= ExcelUtils.getCellData(4, 4);
		String sSelectProfile			= ExcelUtils.getCellData(4, 5);
		String sPlatform				= ExcelUtils.getCellData(4, 6);
		String sOS						= ExcelUtils.getCellData(4, 7);
		String sOSVersion				= ExcelUtils.getCellData(4, 8);
		String sAssignTo				= ExcelUtils.getCellData(4, 9);
		String sSummary					= ExcelUtils.getCellData(4, 10);
		String sDescription				= ExcelUtils.getCellData(4, 11);
		String sAdditionalInformation	= ExcelUtils.getCellData(4, 12);
		String sUploadFile				= ExcelUtils.getCellData(4, 13);
		String sViewStatus				= ExcelUtils.getCellData(4, 14);
		String sMensagem				= ExcelUtils.getCellData(4, 15);
		ReportIssuePage reportIssuePage = PageFactory.initElements(driver, ReportIssuePage.class);
		reportIssuePage.inserirCategoria(sCategory);
		reportIssuePage.inserirReprodutividade(sReproducibility);
		reportIssuePage.inserirSeveridade(sSeverity);
		reportIssuePage.inserirPrioridade(sPriority);
		if (sSelectProfile.isEmpty()){
			reportIssuePage.inserirPlataforma(sPlatform);
			reportIssuePage.inserirOS(sOS);
			reportIssuePage.inserirVersaoOS(sOSVersion);
		} else {
			reportIssuePage.inserirPerfil(sSelectProfile);
		}
		reportIssuePage.inserirAtribuicao(sAssignTo);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC04_ReportIssue_02_Campos001");
		reportIssuePage.inserirSumario(sSummary);
		reportIssuePage.inserirDescricao(sDescription);
		reportIssuePage.inserirInfoAdicional(sAdditionalInformation);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC04_ReportIssue_03_Campos002");
		reportIssuePage.selecionarArquivo(sUploadFile);
		reportIssuePage.selecionarVisaoSituacao(sViewStatus);
		reportIssuePage.naoContinuarRelatando();
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC04_ReportIssue_04_Campos003");
		reportIssuePage.registrarIssue();
		assertTrue(reportIssuePage.existe(sMensagem));
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC04_ReportIssue_05_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou", 4, 16);
		BaseTest.CaptureScreen(driver, pScreenshotPathIssues, "TC04_ReportIssue_06_finishTest");
	}

	
}
