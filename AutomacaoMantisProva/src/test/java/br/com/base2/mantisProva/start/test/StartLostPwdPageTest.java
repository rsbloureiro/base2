package br.com.base2.mantisProva.start.test;


import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.support.PageFactory;

import br.com.base2.mantisProva.start.LoginPage;
import br.com.base2.mantisProva.start.LostPwdPage;
import br.com.base2.mantisProva.util.BaseTest;
import br.com.base2.mantisProva.util.ExcelUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StartLostPwdPageTest extends UtilPageTest{
	
	@Before
	public void iniciaTeste(){
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.recuperarSenha();
	}
		
	@Test
	public void TC01_UsuarioEmailInvalido() throws Exception {
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC01_LostPwdPage_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados,"LostPwdPage");
		String sUsuario  = ExcelUtils.getCellData(1, 1);
		String sEmail	 = ExcelUtils.getCellData(1, 2);
		String sMensagem = ExcelUtils.getCellData(1, 3);
		LostPwdPage lostPwdPage = PageFactory.initElements(driver, LostPwdPage.class);
		lostPwdPage.resetSenha(sUsuario, sEmail);
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC01_LostPwdPage_02_entradaUsuarioEmail");
		lostPwdPage.enviarDados();
		assertTrue(lostPwdPage.existe(sMensagem));
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC01_LostPwdPage_03_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou",1, 4);
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC01_LostPwdPage_04_finishTest");
	}
	
	@Test
	public void TC02_UsuarioEmailNaoCadastrado() throws Exception {
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC02_LostPwdPage_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados,"LostPwdPage");
		String sUsuario  = ExcelUtils.getCellData(2, 1);
		String sEmail	 = ExcelUtils.getCellData(2, 2);
		String sMensagem = ExcelUtils.getCellData(2, 3);
		LostPwdPage lostPwdPage = PageFactory.initElements(driver, LostPwdPage.class);
		lostPwdPage.resetSenha(sUsuario, sEmail);
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC02_LostPwdPage_02_entradaUsuarioEmail");
		lostPwdPage.enviarDados();
		assertTrue(lostPwdPage.existe(sMensagem));
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC02_LostPwdPage_03_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou",2, 4);
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC02_LostPwdPage_04_finishTest");
	}
	
	@Test
	public void TC03_UsuarioEmailEmBranco() throws Exception {
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC03_LostPwdPage_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados,"LostPwdPage");
		String sUsuario  = ExcelUtils.getCellData(3, 1);
		String sEmail	 = ExcelUtils.getCellData(3, 2);
		String sMensagem = ExcelUtils.getCellData(3, 3);
		LostPwdPage lostPwdPage = PageFactory.initElements(driver, LostPwdPage.class);
		lostPwdPage.resetSenha(sUsuario, sEmail);
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC03_LostPwdPage_02_entradaUsuarioEmail");
		lostPwdPage.enviarDados();
		assertTrue(lostPwdPage.existe(sMensagem));
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC03_LostPwdPage_03_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou",3, 4);
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC03_LostPwdPage_04_finishTest");
	}
	
	@Test
	public void TC04_LoginSenhaCorretos() throws Exception {
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC04_LostPwdPage_01_startBrowser");
		ExcelUtils.setExcelFile(pFonteDados,"LostPwdPage");
		String sUsuario  = ExcelUtils.getCellData(4, 1);
		String sEmail	 = ExcelUtils.getCellData(4, 2);
		String sMensagem = ExcelUtils.getCellData(4, 3);
		LostPwdPage lostPwdPage = PageFactory.initElements(driver, LostPwdPage.class);
		lostPwdPage.resetSenha(sUsuario, sEmail);
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC04_LostPwdPage_02_entradaUsuarioEmail");
		lostPwdPage.enviarDados();
		assertTrue(lostPwdPage.existe(sMensagem));
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC04_LostPwdPage_03_mensagemValidacao");
		ExcelUtils.setCellData(pFonteDados, "Passou",4, 4);
		BaseTest.CaptureScreen(driver, pScreenshotPathStart, "TC04_LostPwdPage_04_finishTest");
	}
	
	
	
	
	
	
	@After
	public void finaliza() {
		if(driver != null) {
			driver.quit();
		}
	}
	
}
