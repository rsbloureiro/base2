package br.com.base2.mantisProva.start;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import br.com.base2.mantisProva.util.BaseTest;

public class LostPwdPage extends BaseTest{
	 
	@FindBy(how=How.NAME,using="username")
	WebElement txtUsuario;

	@FindBy(how=How.NAME,using="email")
	WebElement txtEmail;
	
	@FindBy(how=How.XPATH,using="//input[@class='button' and @value='Submit']")
	WebElement btnEnviar;
	
	@FindBy(how=How.LINK_TEXT,using="Login")
	WebElement linkVoltarLogin;
	
	@FindBy(how=How.LINK_TEXT,using="Proceed")
	WebElement linkProsseguir;

	
	public LostPwdPage(WebDriver driver) {
		super(driver);
	}
		
	public void resetSenha(String loginUsuario, String emailUsuario) {
		txtUsuario.clear();
		txtUsuario.sendKeys(loginUsuario);
		txtEmail.clear();
		txtEmail.sendKeys(emailUsuario);
	}
		
	public void enviarDados() {
		btnEnviar.click();		
	}
	
	public void voltarTelaLogin() {
		linkVoltarLogin.click();
	}

	public void prosseguir() {
		linkProsseguir.click();
	}
	
}
