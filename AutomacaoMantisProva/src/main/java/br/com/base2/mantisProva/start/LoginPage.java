package br.com.base2.mantisProva.start;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import br.com.base2.mantisProva.util.BaseTest;

public class LoginPage extends BaseTest{
	
	@FindBy(how=How.NAME,using="username")
	WebElement txtUsuario;

	@FindBy(how=How.NAME,using="password")
	WebElement txtSenha;	
	
	@FindBy(how=How.NAME,using="perm_login")
	WebElement chkLembrarLogin;
	
	@FindBy(how=How.NAME,using="secure_session")
	WebElement chkSessaoSegura;
	
	@FindBy(how=How.XPATH,using="//input[@class='button' and @value='Login']")
	WebElement btnLogin;
	
	@FindBy(how=How.LINK_TEXT,using="Lost your password?")
	WebElement linkEsqueceuSenha;
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	public void identificarUsuario(String loginUsuario, String senhaUsuario) {
		txtUsuario.clear();
		txtUsuario.sendKeys(loginUsuario);
		txtSenha.clear();
		txtSenha.sendKeys(senhaUsuario);
	}
		
	public void lembrarLogin() {
		if (!chkLembrarLogin.isSelected()) {
			chkLembrarLogin.click();
		}
	}
	
	public void naoLembrarLogin() {
		if (chkLembrarLogin.isSelected()) {
			chkLembrarLogin.click();
		}
	}
	
	public void ativarSessaoSegura() {
		if (!chkSessaoSegura.isSelected()) {
			chkSessaoSegura.click();
		}
	}
	
	public void inativarSessaoSegura() {
		if (chkSessaoSegura.isSelected()) {
			chkSessaoSegura.click();
		}
	}
	
	public void logarSistema() {
		btnLogin.click();		
	}
	
	public void recuperarSenha() {
		linkEsqueceuSenha.click();
	}
	
}
