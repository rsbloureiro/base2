package br.com.base2.mantisProva.util;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class StartBrowser {
	
	static WebDriver driver;
	static ThreadLocal<RemoteWebDriver> dr = new ThreadLocal<RemoteWebDriver>();
	static RemoteWebDriver remoteDriver;
	static FirefoxProfile profile;
	static String nav;
	
	public static WebDriver selecionarBrowser (String browser, String url, String hub) throws IOException {
		
		DesiredCapabilities capability;
		profile = ConfigFirefoxProfile();
		
		if (browser.equalsIgnoreCase("firefox")) {
			nav = "firefox";
			System.setProperty("webdriver.gecko.driver", "./bin/FirefoxDriver/geckodriver.exe");	
			capability = DesiredCapabilities.firefox();
//			capability.setCapability(FirefoxDriver.PROFILE, profile);
			capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capability.setCapability("acceptInsecureCerts", true);
			driver = new FirefoxDriver(capability);
			
		} else if (browser.equalsIgnoreCase("chrome")) {
			nav = "chrome";
			System.setProperty("webdriver.chrome.driver", "./bin/ChromeDriver/chromedriver.exe");
			driver = new ChromeDriver();
			
		} else if (browser.equalsIgnoreCase("remote")) {
			nav = "remote";
			capability = DesiredCapabilities.firefox();
//			capability.setCapability(FirefoxDriver.PROFILE, profile);
			capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capability.setCapability("firefox_binary", "C:\\Firefox\\firefox.exe");
			capability.setCapability("acceptInsecureCerts", true);
			remoteDriver = new RemoteWebDriver(new URL(hub), capability);
			setWebDriver(remoteDriver);
			
		}
		
		driver = getDriver(); 
		ConfigDriver(url);
		return driver;
		
	}
	
	public static void ConfigDriver(String url) {
		getDriver().get(url);
		getDriver().manage().window().maximize();
		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
		getDriver().manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	public static WebDriver getDriver() {
		if (nav.contains("remote")) //|| nav.contains("android"))
			return dr.get();
		else
			return driver;
	}

	public static FirefoxProfile ConfigFirefoxProfile() {
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.startup.homepage", "about:blank");
		profile.setPreference("startup.homepage_welcome_url", "about:blank");
		profile.setPreference("startup.homepage_welcome_url.additional", "about:blank");
		profile.setPreference("acceptInsecureCerts", true);
		profile.setPreference("plugin.state.flash", 0);
		profile.setAcceptUntrustedCertificates(true);
		profile.setAssumeUntrustedCertificateIssuer(true);
		return profile;
	}
	
	public static void setWebDriver(RemoteWebDriver driver) {
		dr.set(driver);
	}
	
	public void ExecuteJavaScript(String javaScript, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript(javaScript, element);
	}
	
}
