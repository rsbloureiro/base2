package br.com.base2.mantisProva.issues;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import br.com.base2.mantisProva.util.BaseTest;

public class ReportIssuePage extends BaseTest {

	@FindBy(how=How.NAME,using="category_id")
	WebElement cbxCategory;

	@FindBy(how=How.NAME,using="reproducibility")
	WebElement cbxReproducibility;
	
	@FindBy(how=How.NAME,using="severity")
	WebElement cbxSeverity;
	
	@FindBy(how=How.NAME,using="priority")
	WebElement cbxPriority;
	
	@FindBy(how=How.NAME,using="profile_id")
	WebElement cbxProfile;
	
	@FindBy(how=How.ID,using="platform")
	WebElement txtPlatform;
	
	@FindBy(how=How.ID,using="os")
	WebElement txtOS;
	
	@FindBy(how=How.ID,using="os_build")
	WebElement txtOSVersion;
	
	@FindBy(how=How.NAME,using="handler_id")
	WebElement cbxAssignTo;
	
	@FindBy(how=How.NAME,using="summary")
	WebElement txtSummary;
	
	@FindBy(how=How.NAME,using="description")
	WebElement txtDescription;

	@FindBy(how=How.NAME,using="steps_to_reproduce")
	WebElement txtStepsToReproduce;
	
	@FindBy(how=How.NAME,using="additional_info")
	WebElement txtAdditionalInfo;
	
	@FindBy(how=How.ID,using="ufile[]")
	WebElement btnFile;
	
	@FindBy(how=How.XPATH,using="//input[@type='radio' and @name='view_state']/parent::label[contains(.,'%1s')]/input")
	WebElement rdVisaoSituacao;
	
	@FindBy(how=How.ID,using="report_stay")
	WebElement chkReportStay;
	
	@FindBy(how=How.XPATH,using="//input[@class='button' and @value='Submit Report']")
	WebElement btnSubmitReport;
	
	public ReportIssuePage(WebDriver driver) {
		super(driver);
	}
	
	public void inserirCategoria(String categoria) {
		Select select = new Select(cbxCategory);
		if (!categoria.isEmpty()){
			select.selectByVisibleText(categoria);
		}
	}
		
	public void inserirReprodutividade(String reprodutividade) {
		Select select = new Select(cbxReproducibility);
		if (!reprodutividade.isEmpty()) {
			select.selectByVisibleText(reprodutividade);
		}
	}
		
	public void inserirSeveridade(String severidade) {
		Select select = new Select(cbxSeverity);
		if (!severidade.isEmpty()) {
			select.selectByVisibleText(severidade);
		}
	}
	
	public void inserirPrioridade(String prioridade) {
		Select select = new Select(cbxPriority);
		if (!prioridade.isEmpty()) {
			select.selectByVisibleText(prioridade);	
		}
	}
	
	public void inserirPerfil(String perfil) {
		Select select = new Select(cbxProfile);
		if (!perfil.isEmpty()) {
			select.selectByVisibleText(perfil);	
		}
	}
		
	public void inserirPlataforma(String plataforma) {
		txtPlatform.sendKeys(plataforma);
	}
		
	public void inserirOS(String oS) {
		txtOS.sendKeys(oS);
	}
	
	public void inserirVersaoOS(String versaoOS) {
		txtOSVersion.sendKeys(versaoOS);
	}
		
	public void inserirAtribuicao(String atribuicao) {
		Select select = new Select(cbxAssignTo);
		if (!atribuicao.isEmpty()) {
			select.selectByVisibleText(atribuicao);	
		}
	}
		
	public void inserirSumario(String sumario) {
		txtSummary.sendKeys(sumario);
	}
	
	public void inserirDescricao(String descricao) {
		txtDescription.sendKeys(descricao);
	}

	public void inserirPassos(String passos) {
		txtStepsToReproduce.sendKeys(passos);
	}
	
	public void inserirInfoAdicional(String info) {
		txtAdditionalInfo.sendKeys(info);
	}
	
	public void selecionarArquivo(String path) {
		btnFile.sendKeys(path);
	}
	
	public void selecionarVisaoSituacao(String situacaoVisao) {
		String xpath = "";
		if (!situacaoVisao.isEmpty()){
			xpath = String.format(rdVisaoSituacao.getLocation().toString(), situacaoVisao);
			driver.findElements(By.xpath(xpath)).get(0).click();
		}
	}
	
	public void continuarRelatando() {
		if (!chkReportStay.isSelected()) {
			chkReportStay.click();
		}
	}
	
	public void naoContinuarRelatando() {
		if (chkReportStay.isSelected()) {
			chkReportStay.click();
		}
	}
	
	public void registrarIssue() {
		btnSubmitReport.click();
	}
	
}
