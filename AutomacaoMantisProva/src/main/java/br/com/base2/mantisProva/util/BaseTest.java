package br.com.base2.mantisProva.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseTest {

	protected WebDriver driver;
	protected WebDriverWait wait;
	 
	public BaseTest(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
	}
	
	public BaseTest(RemoteWebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
	}
	
	public BaseTest(WebDriver driver) {
		this.driver = driver;
	}
	
	public BaseTest(RemoteWebDriver driver) {
		this.driver = driver;
	}
	
	public void rolarPaginaUp(){
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-400)", "");
	}
	
	public void rolarPaginaDown(){
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,400)", "");
	}
	
	public boolean existe(String string) {
		return driver.getPageSource().contains(string);
	}

	public static void CaptureScreen(WebDriver driver, String path, String screenshotName) throws IOException {
		try {
			TakesScreenshot ts = (TakesScreenshot)driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source, new File(path+screenshotName+".png"));
		} catch (WebDriverException e) {
			e.printStackTrace();
		}

	}
	
	
}
