package br.com.base2.mantisProva.issues;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import br.com.base2.mantisProva.util.BaseTest;

public class MyViewPage extends BaseTest{

	@FindBy(how=How.LINK_TEXT,using="Unassigned")
	WebElement linkUnassigned;

	@FindBy(how=How.LINK_TEXT,using="Resolved")
	WebElement linkResolved;

	@FindBy(how=How.LINK_TEXT,using="Monitored by Me")
	WebElement linkMonitoredByMe;
	
	@FindBy(how=How.LINK_TEXT,using="Reported by Me")
	WebElement linkReportedByMe;
	
	@FindBy(how=How.LINK_TEXT,using="Recently Modified")
	WebElement linkRecentlyModified;
	
	public MyViewPage(WebDriver driver) {
		super(driver);
	}
	
	public void verNaoAtribuido() {
		linkUnassigned.click();
	}
	
	public void verResolvido() {
		linkResolved.click();
	}
	
	public void verMonitoradoPorMim() {
		linkMonitoredByMe.click();
	}
	
	public void verRelatadoPorMim() {
		linkReportedByMe.click();
	}
		
	public void verModificadoRecente() {
		linkRecentlyModified.click();
	}
	
}
